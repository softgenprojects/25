const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createCategory = async (userId, name) => {
  try {
    const category = await prisma.Category.create({
      data: {
        name,
        userId
      }
    });
    return category;
  } catch (error) {
    throw error;
  }
};

const getCategoriesByUser = async (userId) => {
  try {
    const categories = await prisma.Category.findMany({
      where: {
        userId
      }
    });
    return categories;
  } catch (error) {
    throw error;
  }
};

const updateCategory = async (categoryId, data) => {
  try {
    const updatedCategory = await prisma.Category.update({
      where: {
        id: categoryId
      },
      data
    });
    return updatedCategory;
  } catch (error) {
    throw error;
  }
};

const deleteCategory = async (categoryId) => {
  try {
    await prisma.Category.delete({
      where: {
        id: categoryId
      }
    });
    return { message: 'Category deleted successfully.' };
  } catch (error) {
    throw error;
  }
};

module.exports = {
  createCategory,
  getCategoriesByUser,
  updateCategory,
  deleteCategory
};