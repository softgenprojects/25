const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function createTodo(todoData) {
  const { categoryId, ...rest } = todoData;
  try {
    const todo = await prisma.todo.create({
      data: {
        ...rest,
        ...(categoryId && { Category: { connect: { id: categoryId } } })
      },
    });
    return todo;
  } catch (error) {
    throw error;
  }
}

async function getTodosByUser(userId) {
  try {
    const todos = await prisma.todo.findMany({
      where: { userId },
      include: { Category: true }
    });
    return todos;
  } catch (error) {
    throw error;
  }
}

async function updateTodo(id, updateData) {
  const { categoryId, ...rest } = updateData;
  try {
    const todo = await prisma.todo.update({
      where: { id },
      data: {
        ...rest,
        ...(categoryId && { Category: { connect: { id: categoryId } } })
      },
    });
    return todo;
  } catch (error) {
    throw error;
  }
}

async function deleteTodo(id) {
  try {
    await prisma.todo.delete({
      where: { id },
    });
    return { message: 'Todo successfully deleted' };
  } catch (error) {
    throw error;
  }
}

module.exports = { createTodo, getTodosByUser, updateTodo, deleteTodo };
