const bcrypt = require('bcryptjs');
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const createUser = async (name, email, password) => {
  const existingUser = await prisma.User.findUnique({
    where: {
      email,
    },
  });
  if (existingUser) {
    throw new Error('Email already exists.');
  }
  const hashedPassword = await bcrypt.hash(password, 10);
  return prisma.User.create({
    data: {
      name,
      email,
      password: hashedPassword,
    },
  });
};

const getUserByEmail = async (email) => {
  const user = await prisma.User.findUnique({
    where: {
      email,
    },
  });
  return user;
};

const validateUserPassword = async (email, password) => {
  const user = await getUserByEmail(email);
  if (!user) {
    return false;
  }
  const isPasswordValid = await bcrypt.compare(password, user.password);
  return isPasswordValid;
};

module.exports = { createUser, getUserByEmail, validateUserPassword };