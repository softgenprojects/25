const express = require('express');
const router = express.Router();
const { verifyToken } = require('@middleware/AuthMiddleware');
const {
  createCategoryController,
  getCategoriesController,
  updateCategoryController,
  deleteCategoryController
} = require('@controllers/CategoryController');

router.post('/categories', verifyToken, createCategoryController);
router.get('/categories', verifyToken, getCategoriesController);
router.put('/categories/:id', verifyToken, updateCategoryController);
router.delete('/categories/:id', verifyToken, deleteCategoryController);

module.exports = router;