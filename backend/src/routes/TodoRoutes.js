const express = require('express');
const router = express.Router();
const { verifyToken } = require('@middleware/AuthMiddleware');
const { getTodos, createTodo, updateTodo, deleteTodo } = require('@controllers/TodoController');

router.get('/todos', verifyToken, getTodos);
router.post('/todos', verifyToken, createTodo);
router.put('/todos/:id', verifyToken, updateTodo);
router.delete('/todos/:id', verifyToken, deleteTodo);

module.exports = router;