const express = require('express');
const router = express.Router();
const { registerUser, loginUser, getUserDetails } = require('@controllers/UserController');
const { hashPassword } = require('@middleware/AuthMiddleware');

router.post('/register', hashPassword, registerUser);
router.post('/login', loginUser);
router.get('/me', getUserDetails);

module.exports = router;