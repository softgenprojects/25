const { createTodoService, getTodosService, updateTodoService, deleteTodoService } = require('@services/TodoService');

const createTodo = async (req, res) => {
  try {
    const todo = await createTodoService(req.body);
    res.status(201).json(todo);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getTodos = async (req, res) => {
  try {
    const todos = await getTodosService();
    res.status(200).json(todos);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const updateTodo = async (req, res) => {
  try {
    const updatedTodo = await updateTodoService(req.params.id, req.body);
    res.status(200).json(updatedTodo);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deleteTodo = async (req, res) => {
  try {
    await deleteTodoService(req.params.id);
    res.status(204).send();
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

module.exports = { createTodo, getTodos, updateTodo, deleteTodo };