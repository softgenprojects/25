const { createUser, validateUserPassword, getUserByEmail } = require('@services/UserService');
const jwt = require('jsonwebtoken');
const { PrismaClient, PrismaClientKnownRequestError } = require('@prisma/client');
const prisma = new PrismaClient();

const registerUser = async (req, res) => {
  try {
    const { name, email, password } = req.body;
    const user = await createUser(name, email, password);
    const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, { expiresIn: '1h' });
    res.status(201).json({ user, token });
  } catch (error) {
    if (error instanceof PrismaClientKnownRequestError && error.code === 'P2002') {
      res.status(409).json({ message: 'Email already in use.' });
    } else {
      res.status(500).json({ message: 'Unable to register user', error: error.message });
    }
  }
};

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;
    const isValid = await validateUserPassword(email, password);
    if (!isValid) {
      return res.status(401).json({ message: 'Invalid credentials' });
    }
    const user = await getUserByEmail(email);
    const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, { expiresIn: '1h' });
    res.status(200).json({ user, token });
  } catch (error) {
    res.status(500).json({ message: 'Unable to log in', error: error.message });
  }
};

const getUserDetails = async (req, res) => {
  try {
    const user = await getUserByEmail(req.userId);
    res.status(200).json(user);
  } catch (error) {
    res.status(404).json({ message: 'User not found' });
  }
};

module.exports = { registerUser, loginUser, getUserDetails };