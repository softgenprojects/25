const { createCategory, getCategoriesByUser, updateCategory, deleteCategory } = require('@services/CategoryService');

const createCategoryController = async (req, res) => {
  try {
    const userId = req.user.id;
    const { name } = req.body;
    const category = await createCategory(userId, name);
    res.status(201).json(category);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const getCategoriesController = async (req, res) => {
  try {
    const userId = req.user.id;
    const categories = await getCategoriesByUser(userId);
    res.status(200).json(categories);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const updateCategoryController = async (req, res) => {
  try {
    const { categoryId, data } = req.body;
    const updatedCategory = await updateCategory(categoryId, data);
    res.status(200).json(updatedCategory);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const deleteCategoryController = async (req, res) => {
  try {
    const { categoryId } = req.params;
    const result = await deleteCategory(categoryId);
    res.status(200).json(result);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

module.exports = {
  createCategoryController,
  getCategoriesController,
  updateCategoryController,
  deleteCategoryController
};