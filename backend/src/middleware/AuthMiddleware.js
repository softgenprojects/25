const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const verifyToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  if (!authHeader) {
    return res.status(403).send({ message: 'No token provided!' });
  }

  const token = authHeader.split(' ')[1];
  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: 'Unauthorized!' });
    }
    req.userId = decoded.id;
    next();
  });
};

const hashPassword = (req, res, next) => {
  const { password } = req.body;
  if (password) {
    bcrypt.hash(password, 10, (err, hash) => {
      if (err) {
        return res.status(500).send({ message: 'Error hashing password' });
      }
      req.body.password = hash;
      next();
    });
  } else {
    return res.status(400).send({ message: 'Password is required' });
  }
};

module.exports = { verifyToken, hashPassword };