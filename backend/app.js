require('module-alias/register');
const express = require('express');
const cors = require('cors');
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

const app = express();
app.use(express.json());
app.use(cors({
  origin: ['http://localhost:3080', process.env.CORS_FRONTEND_URL, 'http://65.108.219.251:3080'],
  optionsSuccessStatus: 200,
  credentials: true 
}));

app.get('/', (req, res) => {
  res.send('Hello, World!');
});

const TodoRoutes = require('@routes/TodoRoutes');
const UserRoutes = require('@routes/UserRoutes');
const CategoryRoutes = require('@routes/CategoryRoutes');

app.use('/api', TodoRoutes);
app.use('/api', UserRoutes);
app.use('/api', CategoryRoutes);

// Error handling middleware
app.use((error, req, res, next) => {
  res.status(500).json({ message: 'An unexpected error occurred', error: error.message });
});

const port = process.env.BE_PORT || 8080;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
}).on('error', (err) => {
  console.error('Server failed to start:', err);
});

prisma.$connect().then(() => {
  console.log('Database connected successfully.');
}).catch(err => {
  console.error('Error connecting to the database:', err);
  process.exit(1);
});